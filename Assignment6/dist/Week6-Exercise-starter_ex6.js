let baseString = "TestPage";
function populatePage() {
    let containers = document.querySelectorAll(".container"); //get all the containers

    //Part 1: Rewrite the following code using a for/of loop. 
    //Trivial example: This code capitalizes what is in the HTML file
    //and counts the number of charcters
    //reference for/of loops: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of
    //reference for loops: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for
    let count = 0;

    for (let i = count; i < 8; i += 1){
        count = count + containers[i].innerHTML.length;
        containers[i].innerHTML = containers[i].innerHTML.toUpperCase();
    }

    
    //Output Part 1: You do not need to change this.
    let output = document.querySelector("#output");
    output.innerHTML = "Number of characters:" + count;

    //Part 2: Rewrite the following using a loop and a function
    let heights = [4, 5, 3, 7, 6, 10,-2];
    let pointString = "";//start with an empy string
    let x = 0;

    //Replace the following code with a for/of loop.
    //In this loop, call a function that you will write to 
    //help create the string of points
    
    function pointStr(fx , fy , ps){
        return ps + fx + "," + fy + " ";
    }


    for (let i = 0; i < 7; i += 1){
        x = x + 10;
        y = 200 - heights[i] * 20;
        pointString = pointStr(x , y , pointString);
    }
    
    
    let output2 = document.querySelector("#output2");
    output2.setAttribute("points", pointString);


    //Part 3:  Creating things with loop
    // on the html page with loop.
    //mananually, there are other ways of course.
    let data = [30, 70, 110, 150];
    let svgString = '<svg width="500" height="500">';
    x= 0;

    function circleslol(fcx , fcy , fr , ff){
        return '<circle cx="'+fcx+'"'+' cy="'+fcy+'" r="'+fr+'" fill=" '+ff+' " />';
    }

    for (let i = 0; i < 4; i +=1 ){
        x = data[i];
        circleString = circleslol(x , 250 , 20 , "black");
        svgString += circleString;
    }


    svgString += "</svg>";
    let output3 = document.querySelector("#output3");
    output3.innerHTML += svgString;

}




