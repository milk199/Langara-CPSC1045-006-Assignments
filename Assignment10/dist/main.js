/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

const FILLED = "O";
const EMPTY = "X";
const CELL_SIZE = 50;

let grid = new Array(12);

for (let i = 0 ; i < grid.length ; i += 1) {
    grid[i] = new Array(8);
    for (let j = 0; j < 8; j += 1)
    {
        grid[i][j] = EMPTY;
    }
} // 2d array in data

var score = 0;

console.log(grid);

function drawBoard()
{
    let svgString = '<svg width="800" height="1200">';

    for (let i = 0 ; i < grid.length ; i +=1) {
        for (let j = 0 ; j < grid[i].length ; j +=1) {
            var color = "";
            if (grid[i][j] == EMPTY) {
                color = "white";
            } else {
                color = "black";
            }
            svgString += '<rect x='+ j*CELL_SIZE + ' '
                + ' y=' + i*CELL_SIZE 
                + ' width=' +CELL_SIZE 
                + ' height=' +CELL_SIZE
                + ' fill=' + color + ' stroke=blue />'
        }
    }

    document.querySelector("#output").innerHTML = svgString;
} //svg, represting 2d array in rectangles/squares


function update()
{
    removePiece(currentPiece);
    currentPiece.position.y += 1;

    if (checkGrid() == true)
    {
        generatePiece(currentPiece);
    } 
    else 
    {
        // piece on an empty cell on the board
        currentPiece.position.y -= 1;
        generatePiece(currentPiece);
    
        // piece if off the board or hits a cell
        if (currentPiece.position.y <= 1 ){
            clearInterval(timer);
            currentPiece = null;
            document.getElementById("lose").innerHTML = "you died";
            return; 
        }

        var completedLineCount = 0;
        for (var i = 0; i <= 11; i++)
        {
            var isLineFull = true;
            for (var j = 0; j < 8; j++)
            {
                // check if line is completed
                if (grid[i][j] == EMPTY)
                {
                    isLineFull = false;
                    break; 
                }
            }

            if (isLineFull)
            {
                // For score addition
                completedLineCount += 1;
                // remove full line 
                grid.splice(i, 1);
                // insert blank line
                grid.unshift(new Array(8));
                for (var j = 0; j < 8; j++)
                {
                    grid[0][j] = EMPTY;
                }
                i -= 1;
                console.log(score);
            }
            var scoreAdd; 
            if (completedLineCount == 1){
                scoreAdd = 1;
            } else if (completedLineCount == 2) {
                scoreAdd = 10;
            } else {
                scoreAdd = 0;
            }
            
        }
        score += scoreAdd;
        let scoreD = document.querySelector("score1");
        if (score == 0) {
            scoreD = 0; //creating display before score is added
        } else {
            scoreD = score;
        }
        document.getElementById("score1").innerHTML = scoreD;

        currentPiece = spawnPiece();
        currentPiece.position.x = 3; 
    }
    if (score >= 20) { 
        clearInterval(timer);
        timer = setInterval (update, 125);
    } else if (score >= 40) {
        clearInterval(timer);
        timer = setInterval (update, 75);
    } else if (score >= 70) {
        clearInterval(timer);
        timer = setInterval (update, 10);
    }//gameloop
 
    drawBoard();
    //displaying the game with drawBoard visuals
}

var timer = setInterval(update, 200);


// OBJECT
function Offset(x,y)
{
    this.x = x;
    this.y = y;
}

// OBJECT
function SmallPiece()
{
    this.position = new Offset(0,0);
    this.cells = new Array(
        new Offset(0,0)
    );
}

// OBJECT
function RectanglePiece()
{
    this.position = new Offset(0,0);
    this.cells = new Array (
        new Offset(0,0), new Offset(1,0)
    );
}

// OBJECT
function FatPiece()
{
    this.position = new Offset(0,0);
    this.cells = new Array (
        new Offset(0,0), new Offset(0,1), new Offset(1,0), new Offset(1,1)
    );
}
// Creating offsets for a point

function generatePiece(piece) {

    for (var i = 0; i < piece.cells.length; i += 1){
        // grid [ piece position + cell offset ]
        var yPos = piece.position.y; 
        var xPos = piece.position.x; 
        var yOff = piece.cells[i].y; 
        var xOff = piece.cells[i].x; 
        grid[yPos + yOff][xPos + xOff] = FILLED;
    }
}

function removePiece(piece){

    for (var i = 0; i < piece.cells.length; i++){
        // grid [ piece position + cell offset ]
        var yPos = piece.position.y; 
        var xPos = piece.position.x; 
        var yOff = piece.cells[i].y; 
        var xOff = piece.cells[i].x; 
        grid[yPos + yOff][xPos + xOff] = EMPTY;
    }
}



function checkGrid(){
    var yPos = currentPiece.position.y; 
    var xPos = currentPiece.position.x; 
   
    for (var i = 0; i < currentPiece.cells.length; i++) {

        var yOff = currentPiece.cells[i].y; 
        var xOff = currentPiece.cells[i].x; 
        
        var actualPos = new Offset(xPos + xOff, yPos + yOff);
        // Adding positions together to get actual position

        if (actualPos.x >= 0 
            && actualPos.x < 8
            && actualPos.y >= 0 
            && actualPos.y < 12
            && grid[actualPos.y][actualPos.x] == EMPTY)
        {
            // do nothing 
        } else {
            return false;
        }
    }

    return true;
}

function spawnPiece(){
    var rng = Math.random()
    if (rng < 0.33) {
        return new SmallPiece();
    } else if (rng > 0.33 && rng < 0.66) {
        return new RectanglePiece();
    } else {
        return new FatPiece();
    } //spawning pieces 
}

Math.radians = function(degrees) {
    return degrees * Math.PI / 180;
  };

function rotateClock() {
    if (currentPiece == null){
        return; // deny if off board
    }
    for (let i = 0; i < currentPiece.cells.length; i++) {
        var newX = currentPiece.cells[i].x*Math.cos(Math.radians(90)) - currentPiece.cells[i].y*Math.sin(Math.radians(90));
        var newY = currentPiece.cells[i].y*Math.cos(Math.radians(90)) + currentPiece.cells[i].x*Math.sin(Math.radians(90));
        currentPiece.cells[i].x = Math.round(newX); 
        currentPiece.cells[i].y = Math.round(newY);
    } 

}

function rotateCClock() {
    if (currentPiece == null){
        return;
    }
    for (let i = 0; i < currentPiece.cells.length; i++) {
        var newX = currentPiece.cells[i].x*Math.cos(Math.radians(-90)) - currentPiece.cells[i].y*Math.sin(Math.radians(-90));
        var newY = currentPiece.cells[i].y*Math.cos(Math.radians(-90)) + currentPiece.cells[i].x*Math.sin(Math.radians(-90));
        currentPiece.cells[i].x = Math.round(newX);
        currentPiece.cells[i].y = Math.round(newY);
    } //return it to board if off
}

document.addEventListener('keydown', (event) => {
    const keyName = event.key; 
    console.log(keyName); // checking if keys are read

    if (currentPiece == null){
        return;
    }
    
    if (keyName == "ArrowLeft") //Moves left one cell
    {
        removePiece(currentPiece);
        currentPiece.position.x -= 1; 

        if (checkGrid() == true)
        { 
            generatePiece(currentPiece);
        }
        else 
        {
            currentPiece.position.x += 1;
            generatePiece(currentPiece); 
        }
    }
    else if (keyName == "ArrowRight") //Moves right on cell
    {
        removePiece(currentPiece);
        currentPiece.position.x += 1;

        if (checkGrid() == true)
        {
            generatePiece(currentPiece);
        } else {
            currentPiece.position.x -=1;
            generatePiece(currentPiece);
        }
    } else if (keyName == "ArrowUp") //Rotation
    {
        removePiece(currentPiece);
        rotateClock();
        
        if (checkGrid() == true)
        {
            generatePiece(currentPiece);
        } else {
            rotateCClock();
            generatePiece(currentPiece);
        }

    } 
    drawBoard();
  }, false);


// runtime

var a = new SmallPiece();
console.log(a.position.x);
console.log(a.position.y);
a.position.x += 5
var b = new RectanglePiece();
var c = new FatPiece();
var d = new SmallPiece();

var currentPiece = spawnPiece();
currentPiece.position.x = 3;



drawBoard();


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtEQUEwQyxnQ0FBZ0M7QUFDMUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnRUFBd0Qsa0JBQWtCO0FBQzFFO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUF5QyxpQ0FBaUM7QUFDMUUsd0hBQWdILG1CQUFtQixFQUFFO0FBQ3JJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsZ0JBQWdCLGtCQUFrQjtBQUNsQztBQUNBLG1CQUFtQixPQUFPO0FBQzFCO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBLG9CQUFvQixrQkFBa0I7QUFDdEMsd0JBQXdCLHFCQUFxQjtBQUM3QztBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsQ0FBQzs7O0FBR0Q7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsSztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CO0FBQ0E7O0FBRUE7QUFDQSx1QkFBdUIsU0FBUztBQUNoQztBQUNBO0FBQ0EsMkJBQTJCLE9BQU87QUFDbEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDBCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0JBQStCLE9BQU87QUFDdEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUI7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1QkFBdUI7QUFDdkIsU0FBUztBQUNUO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLG9DO0FBQ0E7QUFDQSxzQjtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7O0FBRUE7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBLG1CQUFtQix3QkFBd0I7QUFDM0M7QUFDQSxvQztBQUNBLG9DO0FBQ0Esb0M7QUFDQSxvQztBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSxtQkFBbUIsd0JBQXdCO0FBQzNDO0FBQ0Esb0M7QUFDQSxvQztBQUNBLG9DO0FBQ0Esb0M7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBLHVDO0FBQ0EsdUM7O0FBRUEsbUJBQW1CLCtCQUErQjs7QUFFbEQsMkM7QUFDQSwyQzs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0EsbUJBQW1CLCtCQUErQjtBQUNsRDtBQUNBO0FBQ0EsbUQ7QUFDQTtBQUNBLEs7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsK0JBQStCO0FBQ2xEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0EsOEI7QUFDQSx5QkFBeUI7O0FBRXpCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxxQzs7QUFFQTtBQUNBLFM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTs7QUFFQSxLO0FBQ0E7QUFDQSxHQUFHOzs7QUFHSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOzs7O0FBSUEiLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL2luZGV4LmpzXCIpO1xuIiwiLy9UaGlzIGlzIHRoZSBlbnRyeSBwb2ludCBKYXZhU2NyaXB0IGZpbGUuXHJcbi8vV2VicGFjayB3aWxsIGxvb2sgYXQgdGhpcyBmaWxlIGZpcnN0LCBhbmQgdGhlbiBjaGVja1xyXG4vL3doYXQgZmlsZXMgYXJlIGxpbmtlZCB0byBpdC5cclxuY29uc29sZS5sb2coXCJIZWxsbyB3b3JsZFwiKTtcclxuXHJcbmNvbnN0IEZJTExFRCA9IFwiT1wiO1xyXG5jb25zdCBFTVBUWSA9IFwiWFwiO1xyXG5jb25zdCBDRUxMX1NJWkUgPSA1MDtcclxuXHJcbmxldCBncmlkID0gbmV3IEFycmF5KDEyKTtcclxuXHJcbmZvciAobGV0IGkgPSAwIDsgaSA8IGdyaWQubGVuZ3RoIDsgaSArPSAxKSB7XHJcbiAgICBncmlkW2ldID0gbmV3IEFycmF5KDgpO1xyXG4gICAgZm9yIChsZXQgaiA9IDA7IGogPCA4OyBqICs9IDEpXHJcbiAgICB7XHJcbiAgICAgICAgZ3JpZFtpXVtqXSA9IEVNUFRZO1xyXG4gICAgfVxyXG59IC8vIDJkIGFycmF5IGluIGRhdGFcclxuXHJcbnZhciBzY29yZSA9IDA7XHJcblxyXG5jb25zb2xlLmxvZyhncmlkKTtcclxuXHJcbmZ1bmN0aW9uIGRyYXdCb2FyZCgpXHJcbntcclxuICAgIGxldCBzdmdTdHJpbmcgPSAnPHN2ZyB3aWR0aD1cIjgwMFwiIGhlaWdodD1cIjEyMDBcIj4nO1xyXG5cclxuICAgIGZvciAobGV0IGkgPSAwIDsgaSA8IGdyaWQubGVuZ3RoIDsgaSArPTEpIHtcclxuICAgICAgICBmb3IgKGxldCBqID0gMCA7IGogPCBncmlkW2ldLmxlbmd0aCA7IGogKz0xKSB7XHJcbiAgICAgICAgICAgIHZhciBjb2xvciA9IFwiXCI7XHJcbiAgICAgICAgICAgIGlmIChncmlkW2ldW2pdID09IEVNUFRZKSB7XHJcbiAgICAgICAgICAgICAgICBjb2xvciA9IFwid2hpdGVcIjtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGNvbG9yID0gXCJibGFja1wiO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHN2Z1N0cmluZyArPSAnPHJlY3QgeD0nKyBqKkNFTExfU0laRSArICcgJ1xyXG4gICAgICAgICAgICAgICAgKyAnIHk9JyArIGkqQ0VMTF9TSVpFIFxyXG4gICAgICAgICAgICAgICAgKyAnIHdpZHRoPScgK0NFTExfU0laRSBcclxuICAgICAgICAgICAgICAgICsgJyBoZWlnaHQ9JyArQ0VMTF9TSVpFXHJcbiAgICAgICAgICAgICAgICArICcgZmlsbD0nICsgY29sb3IgKyAnIHN0cm9rZT1ibHVlIC8+J1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI291dHB1dFwiKS5pbm5lckhUTUwgPSBzdmdTdHJpbmc7XHJcbn0gLy9zdmcsIHJlcHJlc3RpbmcgMmQgYXJyYXkgaW4gcmVjdGFuZ2xlcy9zcXVhcmVzXHJcblxyXG5cclxuZnVuY3Rpb24gdXBkYXRlKClcclxue1xyXG4gICAgcmVtb3ZlUGllY2UoY3VycmVudFBpZWNlKTtcclxuICAgIGN1cnJlbnRQaWVjZS5wb3NpdGlvbi55ICs9IDE7XHJcblxyXG4gICAgaWYgKGNoZWNrR3JpZCgpID09IHRydWUpXHJcbiAgICB7XHJcbiAgICAgICAgZ2VuZXJhdGVQaWVjZShjdXJyZW50UGllY2UpO1xyXG4gICAgfSBcclxuICAgIGVsc2UgXHJcbiAgICB7XHJcbiAgICAgICAgLy8gcGllY2Ugb24gYW4gZW1wdHkgY2VsbCBvbiB0aGUgYm9hcmRcclxuICAgICAgICBjdXJyZW50UGllY2UucG9zaXRpb24ueSAtPSAxO1xyXG4gICAgICAgIGdlbmVyYXRlUGllY2UoY3VycmVudFBpZWNlKTtcclxuICAgIFxyXG4gICAgICAgIC8vIHBpZWNlIGlmIG9mZiB0aGUgYm9hcmQgb3IgaGl0cyBhIGNlbGxcclxuICAgICAgICBpZiAoY3VycmVudFBpZWNlLnBvc2l0aW9uLnkgPD0gMSApe1xyXG4gICAgICAgICAgICBjbGVhckludGVydmFsKHRpbWVyKTtcclxuICAgICAgICAgICAgY3VycmVudFBpZWNlID0gbnVsbDtcclxuICAgICAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJsb3NlXCIpLmlubmVySFRNTCA9IFwieW91IGRpZWRcIjtcclxuICAgICAgICAgICAgcmV0dXJuOyBcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHZhciBjb21wbGV0ZWRMaW5lQ291bnQgPSAwO1xyXG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDw9IDExOyBpKyspXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB2YXIgaXNMaW5lRnVsbCA9IHRydWU7XHJcbiAgICAgICAgICAgIGZvciAodmFyIGogPSAwOyBqIDwgODsgaisrKVxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAvLyBjaGVjayBpZiBsaW5lIGlzIGNvbXBsZXRlZFxyXG4gICAgICAgICAgICAgICAgaWYgKGdyaWRbaV1bal0gPT0gRU1QVFkpXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgaXNMaW5lRnVsbCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrOyBcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKGlzTGluZUZ1bGwpXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIC8vIEZvciBzY29yZSBhZGRpdGlvblxyXG4gICAgICAgICAgICAgICAgY29tcGxldGVkTGluZUNvdW50ICs9IDE7XHJcbiAgICAgICAgICAgICAgICAvLyByZW1vdmUgZnVsbCBsaW5lIFxyXG4gICAgICAgICAgICAgICAgZ3JpZC5zcGxpY2UoaSwgMSk7XHJcbiAgICAgICAgICAgICAgICAvLyBpbnNlcnQgYmxhbmsgbGluZVxyXG4gICAgICAgICAgICAgICAgZ3JpZC51bnNoaWZ0KG5ldyBBcnJheSg4KSk7XHJcbiAgICAgICAgICAgICAgICBmb3IgKHZhciBqID0gMDsgaiA8IDg7IGorKylcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBncmlkWzBdW2pdID0gRU1QVFk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpIC09IDE7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhzY29yZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdmFyIHNjb3JlQWRkOyBcclxuICAgICAgICAgICAgaWYgKGNvbXBsZXRlZExpbmVDb3VudCA9PSAxKXtcclxuICAgICAgICAgICAgICAgIHNjb3JlQWRkID0gMTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChjb21wbGV0ZWRMaW5lQ291bnQgPT0gMikge1xyXG4gICAgICAgICAgICAgICAgc2NvcmVBZGQgPSAxMDtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHNjb3JlQWRkID0gMDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBcclxuICAgICAgICB9XHJcbiAgICAgICAgc2NvcmUgKz0gc2NvcmVBZGQ7XHJcbiAgICAgICAgbGV0IHNjb3JlRCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJzY29yZTFcIik7XHJcbiAgICAgICAgaWYgKHNjb3JlID09IDApIHtcclxuICAgICAgICAgICAgc2NvcmVEID0gMDsgLy9jcmVhdGluZyBkaXNwbGF5IGJlZm9yZSBzY29yZSBpcyBhZGRlZFxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHNjb3JlRCA9IHNjb3JlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInNjb3JlMVwiKS5pbm5lckhUTUwgPSBzY29yZUQ7XHJcblxyXG4gICAgICAgIGN1cnJlbnRQaWVjZSA9IHNwYXduUGllY2UoKTtcclxuICAgICAgICBjdXJyZW50UGllY2UucG9zaXRpb24ueCA9IDM7IFxyXG4gICAgfVxyXG4gICAgaWYgKHNjb3JlID49IDIwKSB7IFxyXG4gICAgICAgIGNsZWFySW50ZXJ2YWwodGltZXIpO1xyXG4gICAgICAgIHRpbWVyID0gc2V0SW50ZXJ2YWwgKHVwZGF0ZSwgMTI1KTtcclxuICAgIH0gZWxzZSBpZiAoc2NvcmUgPj0gNDApIHtcclxuICAgICAgICBjbGVhckludGVydmFsKHRpbWVyKTtcclxuICAgICAgICB0aW1lciA9IHNldEludGVydmFsICh1cGRhdGUsIDc1KTtcclxuICAgIH0gZWxzZSBpZiAoc2NvcmUgPj0gNzApIHtcclxuICAgICAgICBjbGVhckludGVydmFsKHRpbWVyKTtcclxuICAgICAgICB0aW1lciA9IHNldEludGVydmFsICh1cGRhdGUsIDEwKTtcclxuICAgIH0vL2dhbWVsb29wXHJcbiBcclxuICAgIGRyYXdCb2FyZCgpO1xyXG4gICAgLy9kaXNwbGF5aW5nIHRoZSBnYW1lIHdpdGggZHJhd0JvYXJkIHZpc3VhbHNcclxufVxyXG5cclxudmFyIHRpbWVyID0gc2V0SW50ZXJ2YWwodXBkYXRlLCAyMDApO1xyXG5cclxuXHJcbi8vIE9CSkVDVFxyXG5mdW5jdGlvbiBPZmZzZXQoeCx5KVxyXG57XHJcbiAgICB0aGlzLnggPSB4O1xyXG4gICAgdGhpcy55ID0geTtcclxufVxyXG5cclxuLy8gT0JKRUNUXHJcbmZ1bmN0aW9uIFNtYWxsUGllY2UoKVxyXG57XHJcbiAgICB0aGlzLnBvc2l0aW9uID0gbmV3IE9mZnNldCgwLDApO1xyXG4gICAgdGhpcy5jZWxscyA9IG5ldyBBcnJheShcclxuICAgICAgICBuZXcgT2Zmc2V0KDAsMClcclxuICAgICk7XHJcbn1cclxuXHJcbi8vIE9CSkVDVFxyXG5mdW5jdGlvbiBSZWN0YW5nbGVQaWVjZSgpXHJcbntcclxuICAgIHRoaXMucG9zaXRpb24gPSBuZXcgT2Zmc2V0KDAsMCk7XHJcbiAgICB0aGlzLmNlbGxzID0gbmV3IEFycmF5IChcclxuICAgICAgICBuZXcgT2Zmc2V0KDAsMCksIG5ldyBPZmZzZXQoMSwwKVxyXG4gICAgKTtcclxufVxyXG5cclxuLy8gT0JKRUNUXHJcbmZ1bmN0aW9uIEZhdFBpZWNlKClcclxue1xyXG4gICAgdGhpcy5wb3NpdGlvbiA9IG5ldyBPZmZzZXQoMCwwKTtcclxuICAgIHRoaXMuY2VsbHMgPSBuZXcgQXJyYXkgKFxyXG4gICAgICAgIG5ldyBPZmZzZXQoMCwwKSwgbmV3IE9mZnNldCgwLDEpLCBuZXcgT2Zmc2V0KDEsMCksIG5ldyBPZmZzZXQoMSwxKVxyXG4gICAgKTtcclxufVxyXG4vLyBDcmVhdGluZyBvZmZzZXRzIGZvciBhIHBvaW50XHJcblxyXG5mdW5jdGlvbiBnZW5lcmF0ZVBpZWNlKHBpZWNlKSB7XHJcblxyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBwaWVjZS5jZWxscy5sZW5ndGg7IGkgKz0gMSl7XHJcbiAgICAgICAgLy8gZ3JpZCBbIHBpZWNlIHBvc2l0aW9uICsgY2VsbCBvZmZzZXQgXVxyXG4gICAgICAgIHZhciB5UG9zID0gcGllY2UucG9zaXRpb24ueTsgXHJcbiAgICAgICAgdmFyIHhQb3MgPSBwaWVjZS5wb3NpdGlvbi54OyBcclxuICAgICAgICB2YXIgeU9mZiA9IHBpZWNlLmNlbGxzW2ldLnk7IFxyXG4gICAgICAgIHZhciB4T2ZmID0gcGllY2UuY2VsbHNbaV0ueDsgXHJcbiAgICAgICAgZ3JpZFt5UG9zICsgeU9mZl1beFBvcyArIHhPZmZdID0gRklMTEVEO1xyXG4gICAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiByZW1vdmVQaWVjZShwaWVjZSl7XHJcblxyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBwaWVjZS5jZWxscy5sZW5ndGg7IGkrKyl7XHJcbiAgICAgICAgLy8gZ3JpZCBbIHBpZWNlIHBvc2l0aW9uICsgY2VsbCBvZmZzZXQgXVxyXG4gICAgICAgIHZhciB5UG9zID0gcGllY2UucG9zaXRpb24ueTsgXHJcbiAgICAgICAgdmFyIHhQb3MgPSBwaWVjZS5wb3NpdGlvbi54OyBcclxuICAgICAgICB2YXIgeU9mZiA9IHBpZWNlLmNlbGxzW2ldLnk7IFxyXG4gICAgICAgIHZhciB4T2ZmID0gcGllY2UuY2VsbHNbaV0ueDsgXHJcbiAgICAgICAgZ3JpZFt5UG9zICsgeU9mZl1beFBvcyArIHhPZmZdID0gRU1QVFk7XHJcbiAgICB9XHJcbn1cclxuXHJcblxyXG5cclxuZnVuY3Rpb24gY2hlY2tHcmlkKCl7XHJcbiAgICB2YXIgeVBvcyA9IGN1cnJlbnRQaWVjZS5wb3NpdGlvbi55OyBcclxuICAgIHZhciB4UG9zID0gY3VycmVudFBpZWNlLnBvc2l0aW9uLng7IFxyXG4gICBcclxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgY3VycmVudFBpZWNlLmNlbGxzLmxlbmd0aDsgaSsrKSB7XHJcblxyXG4gICAgICAgIHZhciB5T2ZmID0gY3VycmVudFBpZWNlLmNlbGxzW2ldLnk7IFxyXG4gICAgICAgIHZhciB4T2ZmID0gY3VycmVudFBpZWNlLmNlbGxzW2ldLng7IFxyXG4gICAgICAgIFxyXG4gICAgICAgIHZhciBhY3R1YWxQb3MgPSBuZXcgT2Zmc2V0KHhQb3MgKyB4T2ZmLCB5UG9zICsgeU9mZik7XHJcbiAgICAgICAgLy8gQWRkaW5nIHBvc2l0aW9ucyB0b2dldGhlciB0byBnZXQgYWN0dWFsIHBvc2l0aW9uXHJcblxyXG4gICAgICAgIGlmIChhY3R1YWxQb3MueCA+PSAwIFxyXG4gICAgICAgICAgICAmJiBhY3R1YWxQb3MueCA8IDhcclxuICAgICAgICAgICAgJiYgYWN0dWFsUG9zLnkgPj0gMCBcclxuICAgICAgICAgICAgJiYgYWN0dWFsUG9zLnkgPCAxMlxyXG4gICAgICAgICAgICAmJiBncmlkW2FjdHVhbFBvcy55XVthY3R1YWxQb3MueF0gPT0gRU1QVFkpXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICAvLyBkbyBub3RoaW5nIFxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHRydWU7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIHNwYXduUGllY2UoKXtcclxuICAgIHZhciBybmcgPSBNYXRoLnJhbmRvbSgpXHJcbiAgICBpZiAocm5nIDwgMC4zMykge1xyXG4gICAgICAgIHJldHVybiBuZXcgU21hbGxQaWVjZSgpO1xyXG4gICAgfSBlbHNlIGlmIChybmcgPiAwLjMzICYmIHJuZyA8IDAuNjYpIHtcclxuICAgICAgICByZXR1cm4gbmV3IFJlY3RhbmdsZVBpZWNlKCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJldHVybiBuZXcgRmF0UGllY2UoKTtcclxuICAgIH0gLy9zcGF3bmluZyBwaWVjZXMgXHJcbn1cclxuXHJcbk1hdGgucmFkaWFucyA9IGZ1bmN0aW9uKGRlZ3JlZXMpIHtcclxuICAgIHJldHVybiBkZWdyZWVzICogTWF0aC5QSSAvIDE4MDtcclxuICB9O1xyXG5cclxuZnVuY3Rpb24gcm90YXRlQ2xvY2soKSB7XHJcbiAgICBpZiAoY3VycmVudFBpZWNlID09IG51bGwpe1xyXG4gICAgICAgIHJldHVybjsgLy8gZGVueSBpZiBvZmYgYm9hcmRcclxuICAgIH1cclxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY3VycmVudFBpZWNlLmNlbGxzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgdmFyIG5ld1ggPSBjdXJyZW50UGllY2UuY2VsbHNbaV0ueCpNYXRoLmNvcyhNYXRoLnJhZGlhbnMoOTApKSAtIGN1cnJlbnRQaWVjZS5jZWxsc1tpXS55Kk1hdGguc2luKE1hdGgucmFkaWFucyg5MCkpO1xyXG4gICAgICAgIHZhciBuZXdZID0gY3VycmVudFBpZWNlLmNlbGxzW2ldLnkqTWF0aC5jb3MoTWF0aC5yYWRpYW5zKDkwKSkgKyBjdXJyZW50UGllY2UuY2VsbHNbaV0ueCpNYXRoLnNpbihNYXRoLnJhZGlhbnMoOTApKTtcclxuICAgICAgICBjdXJyZW50UGllY2UuY2VsbHNbaV0ueCA9IE1hdGgucm91bmQobmV3WCk7IFxyXG4gICAgICAgIGN1cnJlbnRQaWVjZS5jZWxsc1tpXS55ID0gTWF0aC5yb3VuZChuZXdZKTtcclxuICAgIH0gXHJcblxyXG59XHJcblxyXG5mdW5jdGlvbiByb3RhdGVDQ2xvY2soKSB7XHJcbiAgICBpZiAoY3VycmVudFBpZWNlID09IG51bGwpe1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY3VycmVudFBpZWNlLmNlbGxzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgdmFyIG5ld1ggPSBjdXJyZW50UGllY2UuY2VsbHNbaV0ueCpNYXRoLmNvcyhNYXRoLnJhZGlhbnMoLTkwKSkgLSBjdXJyZW50UGllY2UuY2VsbHNbaV0ueSpNYXRoLnNpbihNYXRoLnJhZGlhbnMoLTkwKSk7XHJcbiAgICAgICAgdmFyIG5ld1kgPSBjdXJyZW50UGllY2UuY2VsbHNbaV0ueSpNYXRoLmNvcyhNYXRoLnJhZGlhbnMoLTkwKSkgKyBjdXJyZW50UGllY2UuY2VsbHNbaV0ueCpNYXRoLnNpbihNYXRoLnJhZGlhbnMoLTkwKSk7XHJcbiAgICAgICAgY3VycmVudFBpZWNlLmNlbGxzW2ldLnggPSBNYXRoLnJvdW5kKG5ld1gpO1xyXG4gICAgICAgIGN1cnJlbnRQaWVjZS5jZWxsc1tpXS55ID0gTWF0aC5yb3VuZChuZXdZKTtcclxuICAgIH0gLy9yZXR1cm4gaXQgdG8gYm9hcmQgaWYgb2ZmXHJcbn1cclxuXHJcbmRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2tleWRvd24nLCAoZXZlbnQpID0+IHtcclxuICAgIGNvbnN0IGtleU5hbWUgPSBldmVudC5rZXk7IFxyXG4gICAgY29uc29sZS5sb2coa2V5TmFtZSk7IC8vIGNoZWNraW5nIGlmIGtleXMgYXJlIHJlYWRcclxuXHJcbiAgICBpZiAoY3VycmVudFBpZWNlID09IG51bGwpe1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIFxyXG4gICAgaWYgKGtleU5hbWUgPT0gXCJBcnJvd0xlZnRcIikgLy9Nb3ZlcyBsZWZ0IG9uZSBjZWxsXHJcbiAgICB7XHJcbiAgICAgICAgcmVtb3ZlUGllY2UoY3VycmVudFBpZWNlKTtcclxuICAgICAgICBjdXJyZW50UGllY2UucG9zaXRpb24ueCAtPSAxOyBcclxuXHJcbiAgICAgICAgaWYgKGNoZWNrR3JpZCgpID09IHRydWUpXHJcbiAgICAgICAgeyBcclxuICAgICAgICAgICAgZ2VuZXJhdGVQaWVjZShjdXJyZW50UGllY2UpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgY3VycmVudFBpZWNlLnBvc2l0aW9uLnggKz0gMTtcclxuICAgICAgICAgICAgZ2VuZXJhdGVQaWVjZShjdXJyZW50UGllY2UpOyBcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBlbHNlIGlmIChrZXlOYW1lID09IFwiQXJyb3dSaWdodFwiKSAvL01vdmVzIHJpZ2h0IG9uIGNlbGxcclxuICAgIHtcclxuICAgICAgICByZW1vdmVQaWVjZShjdXJyZW50UGllY2UpO1xyXG4gICAgICAgIGN1cnJlbnRQaWVjZS5wb3NpdGlvbi54ICs9IDE7XHJcblxyXG4gICAgICAgIGlmIChjaGVja0dyaWQoKSA9PSB0cnVlKVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgZ2VuZXJhdGVQaWVjZShjdXJyZW50UGllY2UpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGN1cnJlbnRQaWVjZS5wb3NpdGlvbi54IC09MTtcclxuICAgICAgICAgICAgZ2VuZXJhdGVQaWVjZShjdXJyZW50UGllY2UpO1xyXG4gICAgICAgIH1cclxuICAgIH0gZWxzZSBpZiAoa2V5TmFtZSA9PSBcIkFycm93VXBcIikgLy9Sb3RhdGlvblxyXG4gICAge1xyXG4gICAgICAgIHJlbW92ZVBpZWNlKGN1cnJlbnRQaWVjZSk7XHJcbiAgICAgICAgcm90YXRlQ2xvY2soKTtcclxuICAgICAgICBcclxuICAgICAgICBpZiAoY2hlY2tHcmlkKCkgPT0gdHJ1ZSlcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGdlbmVyYXRlUGllY2UoY3VycmVudFBpZWNlKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByb3RhdGVDQ2xvY2soKTtcclxuICAgICAgICAgICAgZ2VuZXJhdGVQaWVjZShjdXJyZW50UGllY2UpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9IFxyXG4gICAgZHJhd0JvYXJkKCk7XHJcbiAgfSwgZmFsc2UpO1xyXG5cclxuXHJcbi8vIHJ1bnRpbWVcclxuXHJcbnZhciBhID0gbmV3IFNtYWxsUGllY2UoKTtcclxuY29uc29sZS5sb2coYS5wb3NpdGlvbi54KTtcclxuY29uc29sZS5sb2coYS5wb3NpdGlvbi55KTtcclxuYS5wb3NpdGlvbi54ICs9IDVcclxudmFyIGIgPSBuZXcgUmVjdGFuZ2xlUGllY2UoKTtcclxudmFyIGMgPSBuZXcgRmF0UGllY2UoKTtcclxudmFyIGQgPSBuZXcgU21hbGxQaWVjZSgpO1xyXG5cclxudmFyIGN1cnJlbnRQaWVjZSA9IHNwYXduUGllY2UoKTtcclxuY3VycmVudFBpZWNlLnBvc2l0aW9uLnggPSAzO1xyXG5cclxuXHJcblxyXG5kcmF3Qm9hcmQoKTtcclxuIl0sInNvdXJjZVJvb3QiOiIifQ==