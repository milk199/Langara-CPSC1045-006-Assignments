//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

const FILLED = "O";
const EMPTY = "X";
const CELL_SIZE = 50;

let grid = new Array(12);

for (let i = 0 ; i < grid.length ; i += 1) {
    grid[i] = new Array(8);
    for (let j = 0; j < 8; j += 1)
    {
        grid[i][j] = EMPTY;
    }
} // 2d array in data

var score = 0;

console.log(grid);

function drawBoard()
{
    let svgString = '<svg width="800" height="1200">';

    for (let i = 0 ; i < grid.length ; i +=1) {
        for (let j = 0 ; j < grid[i].length ; j +=1) {
            var color = "";
            if (grid[i][j] == EMPTY) {
                color = "white";
            } else {
                color = "black";
            }
            svgString += '<rect x='+ j*CELL_SIZE + ' '
                + ' y=' + i*CELL_SIZE 
                + ' width=' +CELL_SIZE 
                + ' height=' +CELL_SIZE
                + ' fill=' + color + ' stroke=blue />'
        }
    }

    document.querySelector("#output").innerHTML = svgString;
} //svg, represting 2d array in rectangles/squares


function update()
{
    removePiece(currentPiece);
    currentPiece.position.y += 1;

    if (checkGrid() == true)
    {
        generatePiece(currentPiece);
    } 
    else 
    {
        // piece on an empty cell on the board
        currentPiece.position.y -= 1;
        generatePiece(currentPiece);
    
        // piece if off the board or hits a cell
        if (currentPiece.position.y <= 1 ){
            clearInterval(timer);
            currentPiece = null;
            document.getElementById("lose").innerHTML = "you died";
            return; 
        }

        var completedLineCount = 0;
        for (var i = 0; i <= 11; i++)
        {
            var isLineFull = true;
            for (var j = 0; j < 8; j++)
            {
                // check if line is completed
                if (grid[i][j] == EMPTY)
                {
                    isLineFull = false;
                    break; 
                }
            }

            if (isLineFull)
            {
                // For score addition
                completedLineCount += 1;
                // remove full line 
                grid.splice(i, 1);
                // insert blank line
                grid.unshift(new Array(8));
                for (var j = 0; j < 8; j++)
                {
                    grid[0][j] = EMPTY;
                }
                i -= 1;
                console.log(score);
            }
            var scoreAdd; 
            if (completedLineCount == 1){
                scoreAdd = 1;
            } else if (completedLineCount == 2) {
                scoreAdd = 10;
            } else {
                scoreAdd = 0;
            }
            
        }
        score += scoreAdd;
        let scoreD = document.querySelector("score1");
        if (score == 0) {
            scoreD = 0; //creating display before score is added
        } else {
            scoreD = score;
        }
        document.getElementById("score1").innerHTML = scoreD;

        currentPiece = spawnPiece();
        currentPiece.position.x = 3; 
    }
    if (score >= 20) { 
        clearInterval(timer);
        timer = setInterval (update, 125);
    } else if (score >= 40) {
        clearInterval(timer);
        timer = setInterval (update, 75);
    } else if (score >= 70) {
        clearInterval(timer);
        timer = setInterval (update, 10);
    }//gameloop
 
    drawBoard();
    //displaying the game with drawBoard visuals
}

var timer = setInterval(update, 200);


// OBJECT
function Offset(x,y)
{
    this.x = x;
    this.y = y;
}

// OBJECT
function SmallPiece()
{
    this.position = new Offset(0,0);
    this.cells = new Array(
        new Offset(0,0)
    );
}

// OBJECT
function RectanglePiece()
{
    this.position = new Offset(0,0);
    this.cells = new Array (
        new Offset(0,0), new Offset(1,0)
    );
}

// OBJECT
function FatPiece()
{
    this.position = new Offset(0,0);
    this.cells = new Array (
        new Offset(0,0), new Offset(0,1), new Offset(1,0), new Offset(1,1)
    );
}
// Creating offsets for a point

function generatePiece(piece) {

    for (var i = 0; i < piece.cells.length; i += 1){
        // grid [ piece position + cell offset ]
        var yPos = piece.position.y; 
        var xPos = piece.position.x; 
        var yOff = piece.cells[i].y; 
        var xOff = piece.cells[i].x; 
        grid[yPos + yOff][xPos + xOff] = FILLED;
    }
}

function removePiece(piece){

    for (var i = 0; i < piece.cells.length; i++){
        // grid [ piece position + cell offset ]
        var yPos = piece.position.y; 
        var xPos = piece.position.x; 
        var yOff = piece.cells[i].y; 
        var xOff = piece.cells[i].x; 
        grid[yPos + yOff][xPos + xOff] = EMPTY;
    }
}



function checkGrid(){
    var yPos = currentPiece.position.y; 
    var xPos = currentPiece.position.x; 
   
    for (var i = 0; i < currentPiece.cells.length; i++) {

        var yOff = currentPiece.cells[i].y; 
        var xOff = currentPiece.cells[i].x; 
        
        var actualPos = new Offset(xPos + xOff, yPos + yOff);
        // Adding positions together to get actual position

        if (actualPos.x >= 0 
            && actualPos.x < 8
            && actualPos.y >= 0 
            && actualPos.y < 12
            && grid[actualPos.y][actualPos.x] == EMPTY)
        {
            // do nothing 
        } else {
            return false;
        }
    }

    return true;
}

function spawnPiece(){
    var rng = Math.random()
    if (rng < 0.33) {
        return new SmallPiece();
    } else if (rng > 0.33 && rng < 0.66) {
        return new RectanglePiece();
    } else {
        return new FatPiece();
    } //spawning pieces 
}

Math.radians = function(degrees) {
    return degrees * Math.PI / 180;
  };

function rotateClock() {
    if (currentPiece == null){
        return; // deny if off board
    }
    for (let i = 0; i < currentPiece.cells.length; i++) {
        var newX = currentPiece.cells[i].x*Math.cos(Math.radians(90)) - currentPiece.cells[i].y*Math.sin(Math.radians(90));
        var newY = currentPiece.cells[i].y*Math.cos(Math.radians(90)) + currentPiece.cells[i].x*Math.sin(Math.radians(90));
        currentPiece.cells[i].x = Math.round(newX); 
        currentPiece.cells[i].y = Math.round(newY);
    } 

}

function rotateCClock() {
    if (currentPiece == null){
        return;
    }
    for (let i = 0; i < currentPiece.cells.length; i++) {
        var newX = currentPiece.cells[i].x*Math.cos(Math.radians(-90)) - currentPiece.cells[i].y*Math.sin(Math.radians(-90));
        var newY = currentPiece.cells[i].y*Math.cos(Math.radians(-90)) + currentPiece.cells[i].x*Math.sin(Math.radians(-90));
        currentPiece.cells[i].x = Math.round(newX);
        currentPiece.cells[i].y = Math.round(newY);
    } //return it to board if off
}

document.addEventListener('keydown', (event) => {
    const keyName = event.key; 
    console.log(keyName); // checking if keys are read

    if (currentPiece == null){
        return;
    }
    
    if (keyName == "ArrowLeft") //Moves left one cell
    {
        removePiece(currentPiece);
        currentPiece.position.x -= 1; 

        if (checkGrid() == true)
        { 
            generatePiece(currentPiece);
        }
        else 
        {
            currentPiece.position.x += 1;
            generatePiece(currentPiece); 
        }
    }
    else if (keyName == "ArrowRight") //Moves right on cell
    {
        removePiece(currentPiece);
        currentPiece.position.x += 1;

        if (checkGrid() == true)
        {
            generatePiece(currentPiece);
        } else {
            currentPiece.position.x -=1;
            generatePiece(currentPiece);
        }
    } else if (keyName == "ArrowUp") //Rotation
    {
        removePiece(currentPiece);
        rotateClock();
        
        if (checkGrid() == true)
        {
            generatePiece(currentPiece);
        } else {
            rotateCClock();
            generatePiece(currentPiece);
        }

    } 
    drawBoard();
  }, false);


// runtime

var a = new SmallPiece();
console.log(a.position.x);
console.log(a.position.y);
a.position.x += 5
var b = new RectanglePiece();
var c = new FatPiece();
var d = new SmallPiece();

var currentPiece = spawnPiece();
currentPiece.position.x = 3;



drawBoard();
