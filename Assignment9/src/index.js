//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
var SliderH = document.getElementById("sliderH");
var BarN = document.getElementById("NumberBar");

SliderH.oninput = function() {
    myFunction();
    console.log(this.value);
}

BarN.oninput = function () {
    myFunction();
    console.log(this.value);
}

function myFunction() {
    let svgString = '<svg width="500" height="500">';

    function myRect(x , y , w , h){
        return '<rect x=" '+x+' " '+' y=" '+y+' " width=" '+w+' " height=" '+h+' " fill="black" />';
    }

    let data = 0;

    for (let i = 0; i <= BarN.value; i++) {
        data += 20;
        
        rectString = myRect(data , 0 , 10 , (i/BarN.value) * SliderH.value);
        svgString += rectString;
    }

    let output = document.querySelector("#output");
    output.innerHTML = svgString;
}