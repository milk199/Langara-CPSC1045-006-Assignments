//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.

//Exercise 5
let minN = 1;
let maxN = 5;
let x = 3;

if (minN < x < maxN ) {
    console.log("true");
} else {
    console.log("false");
}


// Assignment 5
function myFunction() {
    let score = 0;
    var Ans1 = Number(answer1.value);
    let ans1 = document.querySelector("Question1");
   
    if (Ans1 == 2) {
        ans1 = "correct";
        score = score + 1;
    } else {
        ans1 = "incorrect";
    }
    document.getElementById("Question1").innerHTML = ans1;

    var Ans2 = Number(answer2.value);
    let ans2 = document.querySelector("Question2");

    if (Ans2 == 9) {
        ans2 = "correct";
        score = score + 1;
    } else {
        ans2 = "incorrect";
    }
    document.getElementById("Question2").innerHTML = ans2;

    var Ans3 = Number(answer3.value);
    let ans3 = document.querySelector("Question3");

    if (Ans3 == 5) {
        ans3 = "correct";
        score = score + 1;
    } else {
        ans3 = "incorrect";
    }
    document.getElementById("Question3").innerHTML = ans3;

    var Ans4 = Number(answer4.value);
    let ans4 = document.querySelector("Question4");

    if (Ans4 == 5) {
        ans4 = "correct";
        score = score + 1;
    } else {
        ans4 = "incorrect";
    }
    document.getElementById("Question4").innerHTML = ans4;

    var Ans5 = Number(answer5.value);
    let ans5 = document.querySelector("Question5");

    if (Ans5 == 3) {
        ans5 = "correct";
        score = score + 1;
    } else {
        ans5 = "incorrect";
    }
    document.getElementById("Question5").innerHTML = ans5;
    
    let CANS = document.querySelector("CorrectAns");
    if (score == 0) {
        CANS = "0/5 Correct";
    } else if (score == 1) {
        CANS = "1/5 Correct";
    } else if (score == 2) {
        CANS = "2/5 Correct";
    } else if (score == 3) {
        CANS = "3/5 Correct";
    } else if (score == 4) {
        CANS = "4/5 Correct";
    } else if (score == 5) {
        CANS = "5/5 Correct Woohoo!!";
    }
    document.getElementById("CorrectAns").innerHTML = CANS;
}
