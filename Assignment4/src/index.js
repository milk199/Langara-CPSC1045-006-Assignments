//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("hello world");

function update(){
let sliderInput = document.querySelector("#myInputX");
console.log("sliderInput",sliderInput);
console.log("slider.value",sliderInput.value);
console.log("slider.value",Number(sliderInput.value));

let myVar = Number(sliderInput.value);

let myX = document.querySelector("#myInputX");
let myY = document.querySelector("#myInputY");
let myR = document.querySelector("#myInputR");
let newVar = 100+myVar;
console.log("newVar",newVar);
myX.setAttribute("width", newVar);
myY.setAttribute("height", newVar);
myR.setAttribute("r", newVar);

let translateString = translate("1+myX" "1+myY");
let rotateString = rotate("1+myR");
let transformString = translateString + rotateString;

}

window.update = update